package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/streadway/amqp"
)

var ch *amqp.Channel

const endpoint = "ping"

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func logOnError(err error, msg string) {
	if err != nil {
		log.Printf("Error %s: %s", msg, err)
	}
}

func pingHandler(w http.ResponseWriter, r *http.Request) {
	content := r.URL.Path[len(fmt.Sprintf("/%s/", endpoint)):]
	publish(content)
	fmt.Fprintf(w, fmt.Sprintf("Published %s", content))
}

func init() {
	host := os.Getenv("RABBITMQ_HOST")
	conn, err := amqp.Dial("amqp://guest:guest@" + host + ":5672/")
	failOnError(err, "Failed to connect to RabbitMQ")

	ch, err = conn.Channel()
	failOnError(err, "Failed to open a channel")

	_, err = ch.QueueDeclare(
		endpoint, // name
		false,    // durable
		false,    // delete when unused
		false,    // exclusive
		false,    // no-wait
		nil,      // arguments
	)
	failOnError(err, "Failed to declare a queue")
}

func publish(body string) {
	err := ch.Publish(
		"",       // exchange
		endpoint, // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})

	if err != nil {
		logOnError(err, "Failed to publish a message")
	} else {
		log.Printf(" [x] Sent %s", body)
	}
}

func main() {
	http.HandleFunc(fmt.Sprintf("/%s/", endpoint), pingHandler)
	http.ListenAndServe(":8000", nil)
}
